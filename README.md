
The Global Views Current URL Field module enhances your Drupal Views by providing a dynamic token for the current URL. This token allows you to extract specific URL parts and query parameters.

## Usage:

1. In your View, add a field and select the **Global: Current URL** field formatter.
2. Choose the **Path Type** option to specify whether you want to use internal path if its a path alias.
3. Select the **Output Type** option to customize how the extracted information is displayed.

Use this module when you need to construct a new link which requires current url or its parts or query params.

