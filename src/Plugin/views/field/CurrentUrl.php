<?php

namespace Drupal\views_current_url\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Path\CurrentPathStack;

/**
 * A handler to output view's current url.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("current_url")
 */
class CurrentUrl extends FieldPluginBase {

  /**
   * Definition of path alias manager.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPathManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentPathStack $current_path) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentPathManager = $current_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.current')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {}

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['path_type'] = ['default' => 1];
    $options['output_type'] = ['default' => 1];
    $options['url_part_no'] = ['default' => 0];
    $options['query_param_name'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $form['path_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Path Type'),
      '#default_value' => $this->options['path_type'],
      '#options' => [
        '1' => $this->t('Use Path Alias'),
        '2' => $this->t('Use Internal Path'),
      ],
    ];

    $form['output_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Output Type'),
      '#default_value' => $this->options['output_type'],
      '#options' => [
        '1' => $this->t('Current URL'),
        '2' => $this->t('Current URL without Base Url'),
        '3' => $this->t('Url Part'),
        '4' => $this->t('Query Parameter'),
      ],
    ];

    $form['url_part_no'] = [
      '#type' => 'select',
      '#title' => $this->t('Url Part No'),
      '#default_value' => $this->options['url_part_no'],
      '#options' => range(0, 10),
      '#states' => [
        'visible' => [
          ':input[type=radio][name="options[output_type]"]' => ['value' => '3'],
        ],
      ],
    ];

    $form['query_param_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Query Parameter Name'),
      '#description' => $this->t('Define query parameter name.'),
      '#default_value' => $this->options['query_param_name'],
      '#states' => [
        'visible' => [
          ':input[type=radio][name="options[output_type]"]' => ['value' => '4'],
        ],
      ],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    global $base_url;
    $request = \Drupal::request();

    // Default to current uri.
    $currPath = $request->getRequestUri();
    // If internal path.
    if ($this->options['path_type'] == 2) {
      $currPath = $this->currentPathManager->getPath();
    }

    if ($this->options['output_type'] == 1) {
      return [
        '#plain_text' => $base_url . $currPath,
      ];
    }

    if ($this->options['output_type'] == 2) {
      return [
        '#plain_text' => $currPath,
      ];
    }

    if ($this->options['output_type'] == 3) {
      $args = explode('/', trim($currPath, '/'));
      return [
        '#plain_text' => $args[$this->options['url_part_no']] ?? '',
      ];
    }

    if ($this->options['output_type'] == 4) {
      $args = explode('/', trim($currPath, '/'));
      return [
        '#plain_text' => $request->query->get($this->options['query_param_name'], ''),
      ];
    }
  }

}
