<?php

/**
 * @file
 * Views hook implementations.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_current_url_views_data_alter(&$data) {
  $data['views']['current_url'] = [
    'title' => t('Current url'),
    'help' => t("View's current url"),
    'field' => [
      'id' => 'current_url',
    ],
  ];
}
